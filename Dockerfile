FROM openjdk:17

RUN mkdir -p /opt/shinyproxy
RUN microdnf update \
 && microdnf install --nodocs wget \
 && microdnf clean all \
 && rm -rf /var/cache/yum \
 && wget https://shinyproxy.io/downloads/shinyproxy-3.1.0.jar -O /opt/shinyproxy/shinyproxy.jar

WORKDIR /opt/shinyproxy
CMD ["java", "-jar", "/opt/shinyproxy/shinyproxy.jar"]

